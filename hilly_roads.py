"""
See http://jamie.works/hilly-roads.html for details

This code expects:
boundary_data/ to contain OS Boundary Line data e.g. district_borough_unitary_region.shp
elevation_data/southampton/ to contain EA LiDAR data e.g. su3510_DTM_2M.asc
road_data/ to contain OS Open Roads data e.g. SU_RoadLink.shp
plots/ to which matplotlib plots are saved
"""

import os
import glob
import linecache
import pandas as pd
import numpy as np
import geopandas as gpd
from shapely.ops import linemerge
from shapely.geometry import LineString, MultiLineString, shape
import matplotlib.pyplot as plt
import seaborn as sns

places = [
    {
        "name": "Sheffield",
        "slug": "sheffield",
        "elevation_dir": "sheffield",
        "boundary_type": "ward", # "local_authority" or "ward"
        "boundary_id": ["E05011001", "E05011002", "E05011004", "E05011005", "E05011003"],
        "road_data": "SK_RoadLink.shp",
        "include": False
    },
    {
        "name": "Leeds",
        "slug": "leeds",
        "elevation_dir": "leeds",
        "boundary_type": "local_authority", # "local_authority" or "ward"
        "boundary_id": ["E08000035"],
        "road_data": "SE_RoadLink.shp",
        "include": False
    },
    {
        "name": "Bristol",
        "slug": "bristol",
        "elevation_dir": "bristol",
        "boundary_type": "local_authority", # "local_authority" or "ward"
        "boundary_id": ["E06000023"],
        "road_data": "ST_RoadLink.shp",
        "include": False
    },
    {
        "name": "Edinburgh",
        "slug": "edinburgh",
        "elevation_dir": "edinburgh",
        "boundary_type": "ward", # "local_authority" or "ward"
        "boundary_id": ["S13002935", "S13002929", "S13002930", "S13002931", "S13002922", "S13002923", "S13002932", "S13002921", "S13002924", "S13002925", "S13002927", "S13002928", "S13002933", "S13002926", "S13002934"],
        "road_data": "NT_RoadLink.shp",
        "include": True
    },
    {
        "name": "Bath",
        "slug": "bath",
        "elevation_dir": "bath",
        "boundary_type": "local_authority", # "local_authority" or "ward"
        "boundary_id": ["E06000022"],
        "road_data": "ST_RoadLink.shp",
        "include": False
    },
    {
        "name": "Stockport",
        "slug": "stockport",
        "elevation_dir": "stockport",
        "boundary_type": "local_authority", # "local_authority" or "ward"
        "boundary_id": ["E08000007"],
        "road_data": "SJ_RoadLink.shp",
        "include": False
    },
    {
        "name": "Southampton",
        "slug": "southampton",
        "elevation_dir": "southampton",
        "boundary_type": "local_authority", # "local_authority" or "ward"
        "boundary_id": ["E06000045"],
        "road_data": "SU_RoadLink.shp",
        "include": False
    },
    {
        "name": "Winchester",
        "slug": "winchester",
        "elevation_dir": "winchester",
        "boundary_type": "ward", # "local_authority" or "ward"
        "boundary_id": ["E05011001", "E05011002", "E05011004", "E05011005", "E05011003"],
        "road_data": "SU_RoadLink.shp",
        "include": False
    }
]

def import_elevation(places):
    elevations = []
    for place in places:
        elevation_pkl = os.path.join("elevation_data", place["elevation_dir"], "elevation.pkl")
        if not os.path.isfile(elevation_pkl):
            data_list = []
            file_paths = glob.glob(os.path.join("elevation_data", place["elevation_dir"], "*.asc"))
            for file in file_paths:
                ncols = int(linecache.getline(file, 1).split()[1])
                nrows = int(linecache.getline(file, 2).split()[1])
                xllcorner = int(linecache.getline(file, 3).split()[1])
                yllcorner = int(linecache.getline(file, 4).split()[1])
                cellsize = int(linecache.getline(file, 5).split()[1])
                eastings = list(range(xllcorner, xllcorner + (ncols*cellsize), cellsize))
                northings = list(reversed(range(yllcorner, yllcorner + (nrows*cellsize), cellsize))) # xllcorner is at the bottom of the file, so read from the top, but reverse the indices
                data = pd.read_csv(file, sep=" ", skiprows=6, names=eastings, na_values=-9999) #
                data.index = northings
                data_list.append(data)
            elevation = pd.concat([df.stack() for df in data_list], axis=0).unstack()
            elevation.to_pickle(elevation_pkl)
        else:
            elevation = pd.read_pickle(elevation_pkl)
        elevations.append(elevation)
    return elevations

def import_roads(places):
    place_roads = []
    for place in places:
        roads_pkl = os.path.join("road_data", "{}_roads.pkl".format(place["slug"]))
        if not os.path.isfile(roads_pkl):
            roads = os.path.join("road_data", place["road_data"])
            roads = gpd.read_file(roads)
            if place["boundary_type"] == "local_authority":
                boundary = os.path.join("boundary_data", "district_borough_unitary_region.shp")
                boundary = gpd.read_file(boundary)
            elif place["boundary_type"] == "ward":
                boundary = os.path.join("boundary_data", "district_borough_unitary_ward_region.shp")
                boundary = gpd.read_file(boundary)
            if len(place["boundary_id"]) == 1:
                boundary = boundary.loc[boundary["CODE"] == place["boundary_id"][0]].reset_index()
                clipped_roads = roads[roads["geometry"].within(boundary.iloc[0].geometry)]
            else:
                boundary = boundary.loc[boundary["CODE"].isin(place["boundary_id"])].unary_union
                clipped_roads = roads[roads["geometry"].within(boundary)]
            clipped_roads.to_pickle(roads_pkl)
        else:
            clipped_roads = pd.read_pickle(roads_pkl)
        clipped_roads = clipped_roads.dissolve(by="nameTOID")
        place_roads.append(clipped_roads)
    return place_roads

def plot_contours(place, elevation):
    pal = sns.color_palette("Set2", 13)
    sns.set_palette(pal)
    fig = plt.figure()
    sns.set_style("whitegrid")
    ax = fig.add_subplot(111)
    ax.contour(list(elevation), elevation.index.values, elevation, 10)
    ax.axis("scaled")
    for edge in ["top", "right", "bottom", "left"]:
        ax.spines[edge].set_visible(False)
    plt.xlabel("\nEastings (m)")
    plt.ylabel("Northings (m)\n")
    plt.title("{} contours\n".format(place["name"]))
    plt.savefig(os.path.join("plots", "{}_plot_contour.png".format(place["slug"])), bbox_inches="tight", pad_inches=.5)


def get_road_height(geometry, elevation):
    try:
        lines = linemerge(geometry) # Apply linemerge to ensure segments joined in correct order
    except ValueError:
        lines = geometry
    if type(lines) == LineString:
        lines = [lines]
    new_lines = []
    delta = []
    for line in lines:
        if line.length > 100:
            points = []
            for i in range(0, int(line.length), 50):
                ip = LineString(line).interpolate(i)
                x, y = ip.xy
                x = round(x[0]/2)*2
                y = round(y[0]/2)*2
                try:
                    z = elevation.get_value(y, x)
                except KeyError:
                    z = np.NaN
                points.append((x, y, z))
            new_line = LineString(points)
            max_elev = np.nanmax([point[2] for point in points])
            min_elev = np.nanmin([point[2] for point in points])
            #if np.isnan(max_elev - min_elev):
            #delta.append(None)
            #else:
            delta.append(max_elev - min_elev)
            new_lines.append(new_line)
        else:
            delta.append(np.NaN)
    geom = MultiLineString(new_lines)
    return pd.Series({"geometry_height": geom, "delta": delta})

def plot_plan(place, roads):
    pal = sns.color_palette("Set2", 13)
    sns.set_palette(pal)
    fig = plt.figure()
    sns.set_style("whitegrid")
    ax = fig.add_subplot(111)
    for index, road in roads.iterrows():
        lines = road["geometry"]
        if type(road["geometry"]) == LineString:
            lines = [lines]
        for line in lines:
            x, y = line.xy
            ax.plot(x, y)
    ax.axis("scaled")
    for edge in ["top", "right", "bottom", "left"]:
        ax.spines[edge].set_visible(False)
    plt.xlabel("\nEastings (m)")
    plt.ylabel("Northings (m)\n")
    plt.title("{} roads\n".format(place["name"]))
    plt.savefig(os.path.join("plots", "{}_plot_plan.png".format(place["slug"])), bbox_inches="tight", pad_inches=.5)

def plot_road_detail(place, roads, road_name):
    roads = roads.loc[roads["name1"] == road_name]
    pal = sns.color_palette("Set2", 13)
    fig = plt.figure()
    sns.set_style("whitegrid")
    sns.set_palette(pal)
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    for index, road in roads.iterrows():
        for i, line in enumerate(road["geometry_height"]):
            x, y = line.xy
            ax1.plot(x, y, color=pal[i])
            ax1.scatter(x[1:], y[1:], marker="o", s=20, color=pal[i])
            ax1.scatter(x[0], y[0], marker="x", s=30, color=pal[i])
            for j in range(0, len(x), 2):
                ax1.annotate(str(j*50), xy=(x[j], y[j]), xytext=(x[j]+5, y[j]+5), fontsize=8)
            x = [k * 50 for k in range(0, len(x))]
            z = [coords[2] for coords in list(line.coords)]
            ax2.plot(x, z, color=pal[i])
            ax2.scatter(x[1:], z[1:], marker="o", s=20, color=pal[i])
            ax2.scatter(x[0], z[0], marker="x", s=30, color=pal[i])
    ax1.axis("scaled")
    plt.suptitle(road_name + "\n")
    ax1.set_xlabel("\nEastings (m)")
    ax1.set_ylabel("Northings (m)\n")
    ax2.set_xlabel("\nDistance along road (m)")
    ax2.set_ylabel("Elevation (m)\n")
    for edge in ["top", "right", "bottom", "left"]:
        ax1.spines[edge].set_visible(False)
        ax2.spines[edge].set_visible(False)
    plt.savefig(os.path.join("plots", "{}_{}_plot_road_detail.png".format(place["slug"], road_name)), dpi=300,  bbox_inches="tight", pad_inches=.5)

def plot_elevation(place, roads):
    fig = plt.figure()
    sns.set_style("whitegrid")
    sns.set_palette(sns.color_palette("Set2", 13))
    ax = fig.add_subplot(111)
    for index, road in roads.iterrows():
        for i, line in enumerate(road["geometry_height"]):
            delta_threshold = 40
            if road["delta"][i] > delta_threshold and line.length > 500 and ~np.isnan(line.coords[0][2]) and ~np.isnan(line.coords[-1][2]) and np.isnan(line.coords).sum() < 5:
                x, y = line.xy
                z = [coords[2] for coords in list(line.coords)]
                line_range = range(0, int(line.length) + 50, 50)
                x = [(i * 50)/1000 for i in range(0, len(z))]
                if z[0] > z[-1]:
                    z = list(reversed(z))
                z = [obs - min(z) for obs in z]
                ax.plot(x, z)
                ax.annotate(road["name1"], xy=(x[-1], z[-1]), xytext=(x[-1]+.02, z[-1]-.8), fontsize=8)
    for edge in ["top", "right", "bottom", "left"]:
        ax.spines[edge].set_visible(False)
    plt.xlabel("\nDistance along road (km)")
    plt.ylabel("Change in elevation (m)\n")
    plt.title("{} roads (longer than 0.5km) with change in elevation over {}m\n".format(place["name"], delta_threshold))
    plt.savefig(os.path.join("plots", "{}_plot_elevation.png".format(place["slug"])), dpi=300,  bbox_inches="tight", pad_inches=.5)

def plot_hist(places, place_roads):
    fig = plt.figure()
    pal = sns.color_palette("Set2", 13)
    sns.set_palette(pal)
    sns.set_style("whitegrid")
    sns.set_palette(sns.color_palette("Set2", 13))
    ax = fig.add_subplot(111)
    for i, (place, roads) in enumerate(zip(places, place_roads)):
        if place["include"]:
            deltas = [np.nanmax(d) for d in roads["delta"] if len(d) > np.isnan(d).sum()]
            sns.distplot(deltas, kde=False, label=place["name"], bins=range(20,160,2), color=pal[i], ax=ax, hist_kws=dict(alpha=.8))
    plt.xlim([20,160])
    for edge in ["top", "right", "bottom", "left"]:
        ax.spines[edge].set_visible(False)
    legend = plt.legend(frameon=True)
    legend.get_frame().set_facecolor("w")
    plt.xlabel("\nChange in elevation (m)")
    plt.ylabel("Number of roads\n")
    plt.title("Number of roads by change in elevation\n")
    plt.savefig(os.path.join("plots", "plot_histogram.png"), dpi=300,  bbox_inches="tight", pad_inches=.5)

"""
Main programme
"""

elevations = import_elevation(places)
place_roads = import_roads(places)
for i, (place, elevation, roads) in enumerate(zip(places, elevations, place_roads)):
    if place["include"]:
        new_geom = roads["geometry"].apply(get_road_height, args=(elevation,))
        place_roads[i] = pd.concat([roads, new_geom], axis=1)
for place, elevation, roads in zip(places, elevations, place_roads):
    if place["include"]:
        #plot_contours(place, elevation)
        #plot_plan(place, roads)
        plot_elevation(place, roads)
        #plot_road_detail(place, roads, "Mousehole Lane")
#plot_hist(places, place_roads)
